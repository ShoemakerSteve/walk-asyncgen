import path from 'path';
import fs from 'fs-extra';

const defaultOpts = {
	excludeDirs: false,
	excludeFiles: false,
	excludeExt: false,
	includeDirs: false,
	includeFiles: false,
	includeExt: false,
	printDirs: true,
};

// option types : excludeDirs: RegExp, excludeFiles: RegExp, excludeExt: string (including leading period, ex: '.html')
export default async function* pathsGenerator(dir = process.cwd(), options = defaultOpts) {
	if (options.printDirs) console.log('dir: ' + dir);
	if (!(await fs.pathExists(dir))) throw new Error('Path does not exist');

	let currentDir = await fs.readdir(dir);
	// filter array using options
	if (options.includeDirs || options.includeExt || options.includeDirs) {
		currentDir = currentDir.filter((item) => {
			const itemPath = path.join(dir, item);

			if ((fs.statSync(itemPath)).isFile()) {
				if (options.includeFiles && !options.includeFiles.test(item)) return false;
				if (options.includeExt && !options.includeExt.test(path.extname(item))) return false;
			}
			if (options.includeDirs && (fs.statSync(itemPath)).isDirectory()) {
				return options.includeDirs.test(item);
			}

			return true;
		});
	}
	if (options.excludeFiles || options.excludeExt || options.excludeDirs) {
		currentDir = currentDir.filter((item) => {
			const itemPath = path.join(dir, item);

			if ((fs.statSync(itemPath)).isFile()) {
				if (options.excludeFiles && options.excludeFiles.test(item)) return false;
				if (options.excludeExt && options.excludeExt.test(path.extname(item))) return false;
			}
			if (options.excludeDirs && (fs.statSync(itemPath)).isDirectory()) {
				return !options.excludeDirs.test(item);
			}

			return true;
		});
	}

	const files = [];
	const folders = [];
	for (const item of currentDir) {
		const itemPath = path.join(dir, item);
		if ((await fs.stat(itemPath)).isDirectory()) {
			folders.push(itemPath);
		} else if ((await fs.stat(itemPath)).isFile()) {
			files.push(itemPath);
		} else throw new Error(`${itemPath} is neither a file nor a folder?`);
	}
	for (const file of files) {
		yield file;
	}
	for (const folder of folders) {
		yield* pathsGenerator(folder, options);
	}
}