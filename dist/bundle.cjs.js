'use strict';

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var path = _interopDefault(require('path'));
var fs = _interopDefault(require('fs-extra'));

function _asyncIterator(iterable) {
  var method;

  if (typeof Symbol === "function") {
    if (Symbol.asyncIterator) {
      method = iterable[Symbol.asyncIterator];
      if (method != null) return method.call(iterable);
    }

    if (Symbol.iterator) {
      method = iterable[Symbol.iterator];
      if (method != null) return method.call(iterable);
    }
  }

  throw new TypeError("Object is not async iterable");
}

function _AwaitValue(value) {
  this.wrapped = value;
}

function _AsyncGenerator(gen) {
  var front, back;

  function send(key, arg) {
    return new Promise(function (resolve, reject) {
      var request = {
        key: key,
        arg: arg,
        resolve: resolve,
        reject: reject,
        next: null
      };

      if (back) {
        back = back.next = request;
      } else {
        front = back = request;
        resume(key, arg);
      }
    });
  }

  function resume(key, arg) {
    try {
      var result = gen[key](arg);
      var value = result.value;
      var wrappedAwait = value instanceof _AwaitValue;
      Promise.resolve(wrappedAwait ? value.wrapped : value).then(function (arg) {
        if (wrappedAwait) {
          resume("next", arg);
          return;
        }

        settle(result.done ? "return" : "normal", arg);
      }, function (err) {
        resume("throw", err);
      });
    } catch (err) {
      settle("throw", err);
    }
  }

  function settle(type, value) {
    switch (type) {
      case "return":
        front.resolve({
          value: value,
          done: true
        });
        break;

      case "throw":
        front.reject(value);
        break;

      default:
        front.resolve({
          value: value,
          done: false
        });
        break;
    }

    front = front.next;

    if (front) {
      resume(front.key, front.arg);
    } else {
      back = null;
    }
  }

  this._invoke = send;

  if (typeof gen.return !== "function") {
    this.return = undefined;
  }
}

if (typeof Symbol === "function" && Symbol.asyncIterator) {
  _AsyncGenerator.prototype[Symbol.asyncIterator] = function () {
    return this;
  };
}

_AsyncGenerator.prototype.next = function (arg) {
  return this._invoke("next", arg);
};

_AsyncGenerator.prototype.throw = function (arg) {
  return this._invoke("throw", arg);
};

_AsyncGenerator.prototype.return = function (arg) {
  return this._invoke("return", arg);
};

function _wrapAsyncGenerator(fn) {
  return function () {
    return new _AsyncGenerator(fn.apply(this, arguments));
  };
}

function _awaitAsyncGenerator(value) {
  return new _AwaitValue(value);
}

function _asyncGeneratorDelegate(inner, awaitWrap) {
  var iter = {},
      waiting = false;

  function pump(key, value) {
    waiting = true;
    value = new Promise(function (resolve) {
      resolve(inner[key](value));
    });
    return {
      done: false,
      value: awaitWrap(value)
    };
  }

  if (typeof Symbol === "function" && Symbol.iterator) {
    iter[Symbol.iterator] = function () {
      return this;
    };
  }

  iter.next = function (value) {
    if (waiting) {
      waiting = false;
      return value;
    }

    return pump("next", value);
  };

  if (typeof inner.throw === "function") {
    iter.throw = function (value) {
      if (waiting) {
        waiting = false;
        throw value;
      }

      return pump("throw", value);
    };
  }

  if (typeof inner.return === "function") {
    iter.return = function (value) {
      return pump("return", value);
    };
  }

  return iter;
}

const defaultOpts = {
  excludeDirs: false,
  excludeFiles: false,
  excludeExt: false
}; // option types : excludeDirs: RegExp, excludeFiles: RegExp, excludeExt: string (including leading period, ex: '.html')

function pathsGenerator() {
  return _pathsGenerator.apply(this, arguments);
}

function _pathsGenerator() {
  _pathsGenerator = _wrapAsyncGenerator(function* (dir = process.cwd(), options = defaultOpts) {
    console.log('dir: ' + dir); // console.log('options:');
    // console.log(options);

    if (!(yield _awaitAsyncGenerator(fs.pathExists(dir)))) throw new Error('Path does not exist');
    let currentDir = yield _awaitAsyncGenerator(fs.readdir(dir)); // filter array using options

    if (options.excludeFiles || options.excludeExt || options.excludeDirs) {
      currentDir = currentDir.filter(item => {
        const itemPath = path.join(dir, item);

        if (fs.statSync(itemPath).isFile()) {
          if (options.excludeFiles && options.excludeFiles.test(item)) return false;
          if (options.excludeExt && options.excludeExt.test(path.extname(item))) return false;
        }

        if (options.excludeDirs && fs.statSync(itemPath).isDirectory()) {
          return !options.excludeDirs.test(item);
        }

        return true;
      });
    }

    const files = [];
    const folders = [];

    for (const item of currentDir) {
      const itemPath = path.join(dir, item);

      if ((yield _awaitAsyncGenerator(fs.stat(itemPath))).isDirectory()) {
        folders.push(itemPath);
      } else if ((yield _awaitAsyncGenerator(fs.stat(itemPath))).isFile()) {
        files.push(itemPath);
      } else throw new Error(`${itemPath} is neither a file nor a folder?`);
    }

    for (const file of files) {
      yield file;
    }

    for (const folder of folders) {
      yield* _asyncGeneratorDelegate(_asyncIterator(pathsGenerator(folder, options)), _awaitAsyncGenerator);
    }
  });
  return _pathsGenerator.apply(this, arguments);
}

module.exports = pathsGenerator;
